export default defineI18nConfig(nuxt => ({
  locales: [ 
    {
      code: 'en',
      name: 'En'
    },
    {
      code: 'ru',
      name: 'Ru'
    },
    {
      code: 'lv',
      name: 'Lv'
    }
  ],
  vueI18nLoader: true,
  fallbackLocale: 'en',
  legacy: false,
    locale: 'lv',
    detectBrowserLanguage: {
        useCookie: true,
        cookieKey: 'i18n_redirected',
        redirectOn: 'root',  // recommended
      },
      
    messages: {
      
        en: { welcome: 'Welcome' }, 
        ru: { welcome: 'Привет' }, 
         lv: { welcome: 'Svelky' }
    }  
  }))
  