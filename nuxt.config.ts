export default defineNuxtConfig ({ 
    modules: ['@nuxtjs/tailwindcss', 
    '@pinia/nuxt', 
    '@nuxtjs/i18n'
],
 i18n: {
          vueI18n: './i18n.config.ts' 
            } 
})
