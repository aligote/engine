import { defineStore } from 'pinia'

interface Product {
  id: string
  name: string
  description: string
  price: string,
  category: string
}

interface ProductsState {
  products: Product[]
  error: string | null
}

export const useProductsStore = defineStore('products', {
  state: (): ProductsState => ({
    products: [] as { id: string, name: string, description: string, price: string, category: string }[],
    error: null,
  }),
  actions: {
    async fetchProducts() {
      try {
        const response = await fetch('https://docs.google.com/spreadsheets/d/e/2PACX-1vSCY5ediHlJm1xSsrsc-6lauEZFwH9PkY5yLk-u7ChwILg6DqWGInlUeIEzSaeu0NsRnJUYNJ-p8hAK/pub?gid=71675463&single=true&output=csv')

        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`)
        }

        const data = await response.text()

        const products = data
          .trim()
          .split('\n')
          .slice(1)
          .map((row: string) => {
            const [id, name, description, price, category] = row.split(',')
            return { id, name, description, price, category }
          })

          // console.log(products);


        this.products = products
        this.error = null
      } catch (e) {
        console.error('Failed to fetch products:', e)
        // this.error = e.message
      }
    },
  },
  getters: {
    getProducts: (state: ProductsState) => state.products,
    getCategories: (state) => Array.from(new Set(state.products.map((p) => p.category.trim()))),
    getError: (state: ProductsState) => state.error,
  },
})
